fonts-pecita (5.4-2) unstable; urgency=medium

  * debian/control
    - Update Maintainer address
    - Remove Christian Perrier from Uploaders (Closes: #927596)
    - Use fontforge-nox for build
    - Use dh13
    - Remove obsolete dependencies to otf-pecita
    - Set Standards-Version: 4.5.0
    - Update Vcs-* to point salsa.debian.org
    - Add Rules-Requires-Root: no
  * Add debian/salsa-ci.yml
  * debian/rules
    - Fix to call dh_fixperms

 -- Hideki Yamane <henrich@debian.org>  Thu, 13 Aug 2020 23:10:05 +0900

fonts-pecita (5.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 5.4
    Closes: bug#841521, bug#749377, Thanks to Philippe Cochy.
  * Add Debian watch file to track upstream release.
  * Update copyright information for upstream.
  * debian/control:
    + Reformat the control file (wrap lines).
    + Mark package compliance with Debian Policy 3.9.8, no change needed
      to source.
    + Use secure URL for Vcs-Browser and Vcs-Git field.
  * debian/rules:
    + Drop overriding dh_builddeb target to use xz compression, xz is now
      default compressiong used for building deb files.
    + Fix permission of Pecita.otf by overriding dh_fixperms stage.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sun, 23 Oct 2016 11:57:02 +0530

fonts-pecita (5.2-1) unstable; urgency=medium

  * New upstream version. Closes: #798226

 -- Christian Perrier <bubulle@debian.org>  Sun, 20 Sep 2015 21:39:39 +0200

fonts-pecita (4.3-1) unstable; urgency=low

  * New upstream release. Now include source cod and use upstream's
    build script. Closes: #749450
  * Update Standard to 3.9.5 (checked)
  * Add 'Multi-Arch: foreign

 -- Christian Perrier <bubulle@debian.org>  Sun, 01 Jun 2014 14:28:04 +0200

fonts-pecita (4.1-2) unstable; urgency=low

  * Upload to unstable

 -- Christian Perrier <bubulle@debian.org>  Wed, 15 May 2013 17:05:41 +0200

fonts-pecita (4.1-1) experimental; urgency=low

  * New upstream release. Closes: #706274
  * Fix copyright file format
  * Update Standards to 3.9.4 (checked)
  * Bump debhelper compatibility to 9
  * Drop otf-pecita transitional package
  * Use "Breaks" instead of "Conflicts". Drop "Provides" as it is no
    longer needed (installations should have transitioned since wheezy
    and the package has anyway no reverse dependency.
  * Fix package description formatting.
  * Use git for packaging: adapt Vcs-* fields.

 -- Christian Perrier <bubulle@debian.org>  Sat, 04 May 2013 12:32:24 +0200

fonts-pecita (3.4-1) experimental; urgency=low

  * New upstream version
    - changes the pattern of attachments
    - add Macedonian alphabet
    - replace latin small letter y
    - various additions and adjustments
  * Compress package with xz


 -- Christian Perrier <bubulle@debian.org>  Sat, 01 Sep 2012 19:28:56 +0200

fonts-pecita (3.2-4) unstable; urgency=low

  * Supersede otf-pecita
  * debhelper down compatibility to 8
  * Drop defoma

 -- pecita.net Archive Automatic Signing Key <packager@pecita.net>  Thu, 25 dec 2011 19:00:00 +0200

fonts-pecita (3.2-3) unstable; urgency=low

  * First packaging for Debian Mentors

 -- pecita.net Archive Automatic Signing Key <packager@pecita.net>  Thu, 22 dec 2011 19:00:00 +0200
